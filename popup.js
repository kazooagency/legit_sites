document.body.innerHTML = "Loading";
const request = async (domain) => {
  console.log(domain);
  const response = await fetch(
    "https://spreadsheets.google.com/feeds/cells/1d0qp8SRxPF2L2xqOa9G-aVuNOTwJs0FycMkr534ZFo8/1/public/full?alt=json"
  );
  const json = await response.json();
  const sites = json.feed.entry;
  for (let i = 0; i < sites.length; i += 2) {
    if (sites[i].content.$t === domain) {
      // chrome.browserAction.setIcon({ path: "okay.png" });
      console.log(sites[i + 1].content.$t);
      document.body.innerHTML = sites[i + 1].content.$t;
      return;
    }
  }
  document.body.innerHTML = "website not found";
  console.log("not found");
};

chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
  var tab = tabs[0];
  var url = new URL(tab.url);
  var domain = url.hostname;

  request(
    domain.split(".")[domain.split(".").length - 2] +
      "." +
      domain.split(".")[domain.split(".").length - 1]
  );
});
