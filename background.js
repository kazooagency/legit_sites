const handleRequest = async (url) => {
  const hostname = new URL(url).hostname;

  const trimmedHostname =
    hostname.split(".")[hostname.split(".").length - 2] +
    "." +
    hostname.split(".")[hostname.split(".").length - 1];

  console.log("trimmed hostname", trimmedHostname);
  const response = await fetch(
    "https://spreadsheets.google.com/feeds/cells/1d0qp8SRxPF2L2xqOa9G-aVuNOTwJs0FycMkr534ZFo8/1/public/full?alt=json"
  );
  const json = await response.json();
  const sites = json.feed.entry;
  for (let i = 0; i < sites.length; i += 2) {
    if (sites[i].content.$t === trimmedHostname) {
      chrome.browserAction.setIcon({ path: "okay.png" });
      console.log(sites[i + 1].content.$t);
      return;
    }
  }
  console.log("not found");
};

chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
  chrome.browserAction.setIcon({ path: "loading.png" });
  chrome.tabs.query({ active: true, lastFocusedWindow: true }, (tabs) => {
    let url = tabs[0].url;
    console.log(url);
    handleRequest(url);
  });
});

chrome.tabs.onActivated.addListener(function (activeInfo) {
  chrome.browserAction.setIcon({ path: "loading.png" });
  chrome.tabs.query({ active: true, lastFocusedWindow: true }, (tabs) => {
    let url = tabs[0].url;
    console.log(url);
    handleRequest(url);
  });
});
